import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'

const parseArguments = () => {
    const someArgs = yargs(hideBin(process.argv)).argv
    // @ts-ignore
    const command = someArgs.command || someArgs.c
    // @ts-ignore
    const help = someArgs.h
    // @ts-ignore
    const all = someArgs.a || someArgs.all

    return { all, command, help }
}

export { parseArguments }
