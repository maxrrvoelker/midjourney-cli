import inquirer from 'inquirer'

const askForRepeat = (): Promise<{ REPEAT: string }> => {
    const questions = [
        {
            name: 'REPEAT',
            type: 'input',
            message:
                'How often do you want to repeat? (Basic: 2-4, Standard: 2-10, Pro & Mega: 2-40. Hit enter to skip):',
        },
    ]

    return inquirer.prompt(questions)
}

const getRepeatParameter = async (): Promise<string> => {
    const { REPEAT } = await askForRepeat()

    return REPEAT ? `--repeat ${REPEAT}` : ''
}

export { getRepeatParameter }

