import inquirer from 'inquirer'

const askForTile = (): Promise<{ TILE: boolean }> => {
    const questions = [
        {
            name: 'TILE',
            type: 'confirm',
            message: 'Should the result be tiled (it repeats on the edges)?',
            default: false,
        },
    ]

    return inquirer.prompt(questions)
}

const getTileParameter = async (): Promise<string> => {
    const { TILE } = await askForTile()

    return TILE ? `--tile` : ''
}

export { getTileParameter }
