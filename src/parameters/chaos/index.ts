import inquirer from 'inquirer'

const askForChaos = (): Promise<{ CHAOS: string }> => {
    const questions = [
        {
            name: 'CHAOS',
            type: 'input',
            message:
                'How much should the results vary from each other? (Chaos, values 0-100 allowed):',
        },
    ]

    return inquirer.prompt(questions)
}

const getChaosParameter = async (): Promise<string> => {
    const { CHAOS } = await askForChaos()
    let currentChaos = CHAOS
    const chaosInt = parseInt(currentChaos)

    if (chaosInt < 0 || chaosInt > 100) {
        return await getChaosParameter()
    }

    return currentChaos ? `--chaos ${currentChaos}` : ''
}

export { getChaosParameter }
