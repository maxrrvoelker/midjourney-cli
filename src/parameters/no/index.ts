import inquirer from 'inquirer'

const askForNo = (): Promise<{ NO: string }> => {
    const questions = [
        {
            name: 'NO',
            type: 'input',
            message:
                'What should NOT be included? Write a comma separated list:',
        },
    ]

    return inquirer.prompt(questions)
}

const getNoParameter = async (): Promise<string> => {
    const { NO } = await askForNo()

    return NO ? `--no ${NO}` : ''
}

export { getNoParameter }
