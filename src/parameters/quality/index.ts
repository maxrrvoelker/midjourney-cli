import { versionDoesNotSupportQuality } from '../versions'
import inquirer from 'inquirer'

type QualityList = Array<string>

const getQualitylevels = (): QualityList => ['', '1', '.5', '.25']

const askForQuality = (version: string): Promise<{ QUALITY: string }> => {
    if (versionDoesNotSupportQuality(version)) {
        return Promise.resolve({ QUALITY: '' })
    }

    const questions = [
        {
            name: 'QUALITY',
            type: 'list',
            message: 'Quality (enter for default):',
            default: '',
            choices: getQualitylevels(),
        },
    ]

    return inquirer.prompt(questions)
}

const getQualityParameter = async (version: string): Promise<string> => {
    const { QUALITY } = await askForQuality(version)
    return QUALITY ? `--quality ${QUALITY}` : ''
}

export { getQualityParameter }
