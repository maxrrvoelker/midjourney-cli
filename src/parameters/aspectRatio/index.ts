import inquirer from 'inquirer'

// todo test for aspect >= 1:2 && aspect <= 2:1 for v4
const askForAspectRatio = (): Promise<{ ASPECT_RATIO: string }> => {
    const questions = [
        {
            name: 'ASPECT_RATIO',
            type: 'input',
            message:
                'Aspect Ratio. Format: "width:height" (Enter nothing for default 1:1):',
        },
    ]

    return inquirer.prompt(questions)
}

const getAspectRatioParameter = async (): Promise<string> => {
    const { ASPECT_RATIO } = await askForAspectRatio()

    return ASPECT_RATIO ? `--aspect ${ASPECT_RATIO}` : ''
}

export { askForAspectRatio, getAspectRatioParameter }
