import inquirer from 'inquirer'

const getVersions = () => ({
    VERSION_5_2: '5.2',
    VERSION_NIJI: 'niji',
    VERSION_5_1: '5.1',
    VERSION_5: '5',
    VERSION_4: '4',
    VERSION_3: '3',
    VERSION_2: '2',
    VERSION_1: '1',
})

const getVersionsWithStyleSupport = () => {
    const { VERSION_5_2, VERSION_5_1, VERSION_NIJI, VERSION_4 } = getVersions()

    return { VERSION_5_2, VERSION_5_1, VERSION_NIJI, VERSION_4 }
}

const versionDoesNotSupportStyles = ({
    VERSION,
}: {
    VERSION: string
}): boolean => {
    return !Object.values(getVersionsWithStyleSupport()).find(
        (element) => element === VERSION
    )
}

const getVersionsWithQualitySupport = () => {
    const { VERSION_4, VERSION_5, VERSION_5_1, VERSION_5_2, VERSION_NIJI } =
        getVersions()

    return { VERSION_4, VERSION_5, VERSION_5_1, VERSION_5_2, VERSION_NIJI }
}

const versionDoesNotSupportQuality = (version: string): boolean => {
    return !Object.values(getVersionsWithQualitySupport()).find(
        (element) => element === version
    )
}

const askForVersion = (): Promise<{ VERSION: string }> => {
    const questions = [
        {
            name: 'VERSION',
            type: 'list',
            default: '5.2',
            message: 'version (enter for default):',
            choices: Object.values(getVersions()),
        },
    ]

    return inquirer.prompt(questions)
}

const getVersionParameter = async (givenVersion?: string): Promise<string> => {
    let usedVersion = givenVersion
    const { VERSION_NIJI } = getVersions()

    if (!givenVersion) {
        const { VERSION } = await askForVersion()
        usedVersion = VERSION
    }
    return usedVersion === VERSION_NIJI
        ? '--niji 5'
        : `--version ${usedVersion}`
}

export {
    askForVersion,
    getVersionParameter,
    getVersions,
    getVersionsWithStyleSupport,
    getVersionsWithQualitySupport,
    versionDoesNotSupportStyles,
    versionDoesNotSupportQuality,
}
