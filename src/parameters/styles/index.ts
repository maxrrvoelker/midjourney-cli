import { getVersions, versionDoesNotSupportStyles } from '../versions'
import inquirer from 'inquirer'

type StyleList = Array<string>

const getNijiStyles = (): StyleList => [
    '',
    'cute',
    'scenic',
    'original',
    'expressive',
]

const getVersionFiveStyles = (): StyleList => ['', 'raw']

const getVersionFourStyles = (): StyleList => ['', '4a', '4b', '4c']

const getStylesForVersion = (version: string): StyleList => {
    const { VERSION_NIJI, VERSION_5_2, VERSION_5_1, VERSION_4 } = getVersions()

    if (version === VERSION_NIJI) {
        return getNijiStyles()
    }
    if (version === VERSION_4) {
        return getVersionFourStyles()
    }
    if ([VERSION_5_2, VERSION_5_1].find((element) => element === version)) {
        return getVersionFiveStyles()
    }

    return ['']
}

const askForStyle = (version: string): Promise<{ STYLE: string }> => {
    if (versionDoesNotSupportStyles({ VERSION: version })) {
        return Promise.resolve({ STYLE: '' })
    }

    const questions = [
        {
            name: 'STYLE',
            type: 'list',
            message: 'style (enter for default):',
            default: '',
            choices: getStylesForVersion(version),
        },
    ]

    return inquirer.prompt(questions)
}

const getStyleParameter = async (version: string): Promise<string> => {
    const { STYLE } = await askForStyle(version)
    return STYLE ? `--style ${STYLE}` : ''
}

export { getStyleParameter }
