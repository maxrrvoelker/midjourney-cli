#!/usr/bin/env node

import chalk from 'chalk'
import clipboard from 'node-clipboardy'
import { exit } from 'shelljs'

import { parseArguments } from './argumentsParser'
import { buildPrompt } from './prompt/builder'
import { showHelp } from './help'

const run = async () => {
    const { all, help } = parseArguments()

    if (help) {
        showHelp()
        exit()
    }
    const prompt = await buildPrompt(all)

    console.log(chalk.greenBright(`copied to clipboard! - ${prompt}`))
    clipboard.writeSync(prompt)
}

run()
