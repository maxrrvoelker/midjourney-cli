import chalk from 'chalk'
import { textSync } from 'figlet'

const showHelp = () => {
    const helptext = `Usage: 
  -a, --all                               | get all questions asked, you can set all parameters
  -c <command>, --command=<command>       | replace command with a command type you want to trigger
  -h                                      | show this help
  `
    console.log(chalk.blue(textSync('Midjourney CLI')))
    console.log(chalk.green(helptext))
}

export { showHelp }
