import { askForPrompt } from '../prompt'
import {
    askForVersion,
    getAspectRatioParameter,
    getChaosParameter,
    getNoParameter,
    getQualityParameter,
    getRepeatParameter,
    getStyleParameter,
    getTileParameter,
    getVersionParameter,
} from '../parameters'

const normalisePrompt = (prompt: string): string =>
    prompt.replace(/\s+/g, ' ').trim()

const buildPrompt = async (all: boolean): Promise<string> => {
    const { PROMPT } = await askForPrompt()
    const { VERSION } = await askForVersion()
    const versionParameter = await getVersionParameter(VERSION)
    const styleString = await getStyleParameter(VERSION)
    const noParameter = await getNoParameter()

    if (!all) {
        return normalisePrompt(`
                             /imagine 
                             ${PROMPT} 
                             ${versionParameter} 
                             ${styleString} 
                             ${noParameter}
                             `)
    }

    const qualityParameter = await getQualityParameter(VERSION)
    const aspectParameter = await getAspectRatioParameter()
    const tileParameter = await getTileParameter()
    const chaosParameter = await getChaosParameter()
    const repeatParameter = await getRepeatParameter()

    return normalisePrompt(`
                           /imagine
                           ${PROMPT} 
                           ${versionParameter} 
                           ${styleString} 
                           ${aspectParameter} 
                           ${tileParameter} 
                           ${chaosParameter} 
                           ${qualityParameter} 
                           ${repeatParameter}
                           ${noParameter}
                           `)
}

export { buildPrompt }
