import inquirer from 'inquirer'

const askForPrompt = (): Promise<{ PROMPT: string }> => {
    const questions = [
        {
            name: 'PROMPT',
            type: 'input',
            message: 'What do you want to imagine?',
        },
    ]

    return inquirer.prompt(questions)
}

export { askForPrompt }
