# Midjourney CLI
This tool assists you in writing midjourney prompts. It asks you questions and automatically generates the prompt for you with all parameters set. 

# Usage
Clone this repository, run `npm install` and `npm link`. Then you can call the cli tool by typing in `mid` or `midjourney-cli`.

# Extended questions
Calling the cli tool with `mid -a` or `mid --all` will trigger the extended mode. you will get asked to give a value for every existing parameter, based on the version chosen. 

## Todos
* add profiles
    * store different settings inside profiles. load profiles on cli start
* connect cli directly to your discord
    * send the prompt directly to your discord midjourney bot, see the results on discord
